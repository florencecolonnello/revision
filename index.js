"use strict";

//1) je créer mon tableau
let myArray = [];


//2) je créer une fonction pour remplir dans mon tableau
function giveNumber(maxNumber) {
  for (let i = 1; i <= maxNumber; i++) {
    //je push chaque résultat dans le tableau
    myArray.push(i);
  }
}

// j'appelle ma fonction avec le nombre qu'il me faut
giveNumber(30);
//je verifie dans le navigateur
// console.log(myArray);

function giveSquare(magicArray) {
  let fabulousArray = [];
  for (let y = 0; y < magicArray.length; y++) {
    let square = magicArray[y] * magicArray[y];
    fabulousArray.push(square);
    console.log(`${magicArray[y]} x ${magicArray[y]} = ${square}`);
    
  }
  return fabulousArray;
}

// giveSquare(myArray);

giveSquare(myArray);